cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

# Packages are optional: if they are not present, certain code samples are not compiled
find_package(OpenMP)   # Built-in in CMake
find_package(MPI REQUIRED)      # Built-in in CMake
# find_package(Common)

include(${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 5_STRUCT)
endif()
set(NAME ${DWARF_PREFIX}_JTC_mpi_openmp)

# ==================================================================================================
# C++ compiler settings

find_package(Common)
select_compiler_flags(c_flags
  GNU "-std=c99 -fopenmp -DUSE_OPENMP -DUSE_MPI -O3"
  Intel "-restrict -DUSE_OPENMP -DUSE_MPI")   # "-O3" is controlled by the CMAKE_BUILD_TYPE
set(C_FLAGS ${c_flags})
if("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")
  set(C_FLAGS "${C_FLAGS} -Wall -Wno-comment")
endif()
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")

# set(CMAKE_C_COMPILER "mpiicc")
include_directories(${MPI_INCLUDE_PATH})
link_libraries(m)


if (OPENMP_FOUND AND MPI_FOUND)
#    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MPI_C_FLAGS} ${OpenMP_C_FLAGS}")
    add_executable (${NAME} src/kernels_c.c src/comm_mpi_c.c src/utils_c.c src/jacobi_c.c)
    target_link_libraries(${NAME} ${MPI_LIBRARIES})
    set_target_properties(${NAME} PROPERTIES COMPILE_FLAGS "${c_flags}")
    message("** Enabling '${NAME}': with OpenMP and MPI")
    # next line needs to be uncommented if using test_build.py
    install(TARGETS ${NAME} DESTINATION bin)
    if (MPI_LINK_FLAGS)
	set_target_properties(${NAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS} -lstdc++")
    endif()
else()
     message("## Skipping '${NAME}_omp': no MPI or OpenMP support found")
     dummy_install(${NAME} "MPI_OpenMP")
endif()

unset(NAME)

# ==================================================================================================
