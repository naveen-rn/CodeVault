
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Andrew Sunderland <andrew.sunderland@stfc.ac.uk>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)
project("5_structured")
include(${CMAKE_CURRENT_SOURCE_DIR}/../cmake/common.cmake)

# ==================================================================================================

# Dwarf 5: Structured Grids
message("--------------------")
message("Dwarf 5: Structured Grids: ")
message("--------------------")
set(DWARF_PREFIX 5_STRUCT) # The prefix of the name of the binaries produced

# Add the examples
add_subdirectory(JTC/JTC_openmp)
add_subdirectory(JTC/JTC_mpi_openmp)
# ==================================================================================================




