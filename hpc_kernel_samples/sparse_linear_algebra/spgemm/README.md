CodeVault: Sparse Matrix-Matrix Multiplication
================
# Overview
Sparse matrix-matrix multiplication (SpGEMM) operation, which involves multiplication of two sparse matrices, is used in solving linear programming problems, molecular dynamics simulations (e.g. CP2K), etc. This folder contains samples for performing SpGEMM.

# Contributors & Maintainers
- Cevdet Aykanat (aykanat@cs.bilkent.edu.tr) 
- Kadir Akbudak (kadir.cs@gmail.com) 
- Reha Oguz Selvitopi(reha@cs.bilkent.edu.tr) 

