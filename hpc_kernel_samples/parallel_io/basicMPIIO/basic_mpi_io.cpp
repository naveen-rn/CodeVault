#include "myMPI.hpp"
#include <iostream>
#include <cassert>
#include <string>
#include <stdexcept>
#include <utility>
#include "parse_arguments.h"
#include "plummer.h"

struct Particle
{
  int rank, id;
  double mass;
  double posx, posy, posz;
  double velx, vely, velz;
};

struct Header
{
  char model_name[256];
  size_t nbodies;
  size_t comm_size;
  size_t seed;
};

std::runtime_error error_msg(int rank, std::string error)
{
  using namespace std;
  return runtime_error(std::string("Rank# ")+ to_string(rank)+": " + error + "\n");
}

void read_bodies(const my::MPI::Comm &comm, std::string filename)
{
  using namespace std;

  // 1: Open file
  MPI_File fh;
  const auto err = MPI_File_open(comm.comm(),
      (char*)filename.c_str(),
      MPI_MODE_RDONLY,    // readonly file
      MPI_INFO_NULL,      // no info handle
      &fh);
  if (err != MPI_SUCCESS)
    throw error_msg(comm.rank(),"Unable to open a file '"+filename+"' to read.\n");

  // 2: Read header
  
  Header header;
  MPI_Status status;

  {
    // Here all ranks read the same data, this avoids a MPI_Broadcast call ..
    //   .. should be safe [but I don't know if it is]
    MPI_File_seek(fh,0,MPI_SEEK_SET);
    MPI_File_read(fh,
        &header,
        sizeof(Header),
        MPI_BYTE,
        &status);
    int check_count;
    int count = sizeof(Header);
    MPI_Get_count(&status, MPI_BYTE, &check_count);
    if (count != check_count)
      throw error_msg(comm.rank(),
          string("FATAL: Incorrect number of header bytes was read. Expected ")+to_string(count)+
          " but wrote " + to_string(check_count) + "!\n");
  }
  
  if (comm.is_root())
  {
    cout << " Model info:\n";
    cout << "---------------------------\n";
    cout << "  model name: " << header.model_name << endl;
    cout << "  nbodies   : " << header.nbodies << endl;
    cout << "  comm_size : " << header.comm_size << endl;
    cout << "---------------------------\n";
  }


  // 3: Compute how many particles each rank will read
  const auto bodies_per_rank0 = (header.nbodies+comm.size() - 1)/comm.size();
  auto bodies_per_rank = bodies_per_rank0;


  if (comm.rank() + 1 == comm.size())
    bodies_per_rank += header.nbodies - comm.size()*bodies_per_rank;
  assert(bodies_per_rank > 0);
  


  // sanity check that the split is correct
  const auto nbodies_check = comm.allreduce(static_cast<size_t>(bodies_per_rank), my::MPI::SUM);
  assert(nbodies_check == header.nbodies);

  std::vector<Particle> local_bodies(bodies_per_rank);
  
  const size_t beg = bodies_per_rank0*comm.rank()*sizeof(Particle);
  const size_t end = beg + bodies_per_rank*sizeof(Particle);
  assert(end-beg == bodies_per_rank*sizeof(Particle));
  if (comm.rank() + 1 == comm.size())
    assert(header.nbodies*sizeof(Particle) == end);

  // 4: Similarly to write, the MPI_File_write take count as 'int'
  //    .. thus we need guard it to ensure we can read files > 4GB in size
  size_t chunk_offset = sizeof(Header) + beg;
  size_t chunk_size   = end-beg;
  if (chunk_size > 0)
  {
    MPI_File_seek(fh, chunk_offset, MPI_SEEK_SET);
    MPI_Status status;
    const size_t batchMax = (1U << 30) - 1;
    int loop_count = 1;
    size_t offset = 0;
    while (chunk_size > 0)
    {
      const auto count = static_cast<int>(min(chunk_size,batchMax));
      assert(count > 0);
      MPI_File_read(fh,
          local_bodies.data() + offset,
          count,
          MPI_BYTE,
          &status);

      int check_count;
      MPI_Get_count(&status, MPI_BYTE, &check_count);
      if (count != check_count)
        throw error_msg(comm.rank(),
            string("FATAL: Incorrect number of data bytes was read during loop# ") + 
            to_string(loop_count) + ". Expected "+
            to_string(count)+ " but wrote " + to_string(check_count) + "!\n");
      offset     += count;
      chunk_size -= count;
      loop_count++;
    }
  }

  // 5: Now let's check the correctness;
  if (comm.size() == (int)header.comm_size)
  {
    Plummer plummer(bodies_per_rank, header.seed + comm.rank(), string("plummer"+to_string(comm.rank())).c_str());
    for (int i = 0; i < (int)bodies_per_rank; i++)
    {
      auto &b = local_bodies[i];
      assert(b.rank ==  comm.rank());
      assert(b.id   == i);
      assert(b.mass == plummer.mass[i]);
      assert(b.posx == plummer.pos[i].x);
      assert(b.posy == plummer.pos[i].y);
      assert(b.posz == plummer.pos[i].z);
      assert(b.velx == plummer.vel[i].x);
      assert(b.vely == plummer.vel[i].y);
      assert(b.velz == plummer.vel[i].z);
    }
  }
  else
  {
    const auto bodies_per_old_rank = (header.nbodies + header.comm_size - 1)/header.comm_size;
    for (int i = 0; i < (int)bodies_per_rank; i++)
    {
      const size_t global_idx = bodies_per_rank0*comm.rank() + i;
      const int local_rank = global_idx/bodies_per_old_rank;
      const int local_idx  = global_idx%bodies_per_old_rank;

      auto &b = local_bodies[i];
      assert(b.rank == local_rank);
      assert(b.id   == local_idx);
    }
  }


  MPI_File_close(&fh);
}

void write_bodies(const my::MPI::Comm &comm, std::string filename, int nbodies, std::string model_name)
{
  using namespace std;


  // Split nbodies between the ranks
  auto bodies_per_rank = (nbodies+comm.size() - 1)/comm.size();
  if (comm.rank() + 1 == comm.size())
  {
    bodies_per_rank += nbodies - comm.size()*bodies_per_rank;
  }
  assert(bodies_per_rank > 0);

  // sanity check that the split is correct
  const auto nbodies_check = comm.allreduce(static_cast<size_t>(bodies_per_rank), my::MPI::SUM);
  assert((int)nbodies_check == nbodies);
 
  // Generate reproducible model : will generate tons of output with larger comm.size()
  if (comm.is_root())
    cout << "Generating plummer model ... \n";
  size_t  seed = 19810614;
  Plummer plummer(bodies_per_rank, seed + comm.rank(), string("plummer"+to_string(comm.rank())).c_str());

  // loop over each body and assign values
  vector<Particle> local_bodies(bodies_per_rank);
  for (int i = 0; i < bodies_per_rank; ++i)
  {
    auto &b = local_bodies[i];
    b.rank = comm.rank();
    b.id   = i;
    b.mass = plummer.mass[i];
    b.posx = plummer.pos[i].x;
    b.posy = plummer.pos[i].y;
    b.posz = plummer.pos[i].z;
    b.velx = plummer.vel[i].x;
    b.vely = plummer.vel[i].y;
    b.velz = plummer.vel[i].z;
  } 

  // Open file: use RAW MPI, no abstarction for clarity
  MPI_File fh;     
  // do open_with_delete_on_close/close/open cycle to make sure old file is removed [1]
  //  .. is there a better way to do it
  //  .. [1] http://stackoverflow.com/questions/25481661/how-to-replace-an-existing-file-in-mpi-with-mpi-file-open
  auto err = MPI_File_open(
      comm.comm(),        // communicator
      (char*)filename.c_str(),   // filename
      MPI_MODE_CREATE| MPI_MODE_DELETE_ON_CLOSE | MPI_MODE_WRONLY,   // mode of operation: create file and write it
      MPI_INFO_NULL,      // no info object handle,
      &fh                 // file handle
      );
  MPI_File_close(&fh);
  err = MPI_File_open(
      comm.comm(),        // communicator
      (char*)filename.c_str(),   // filename
      MPI_MODE_CREATE| MPI_MODE_WRONLY,   // mode of operation: create file and write it
      MPI_INFO_NULL,      // no info object handle,
      &fh                 // file handle
      );

  // throw an exception if we failed to open a file
  if (err != MPI_SUCCESS)
    throw error_msg(comm.rank(),"Unable to open a file '"+filename+"' to write.\n");



  // We are now ready to write data, but before we do that
  // we first need to compute displacement for where each ranks
  // will write in parallel
  

  // 1: each rank  gathers  counts_per_rank = bodies_per_rank * sizeof(Particles)  from all ranks
  vector<size_t> counts_per_rank(comm.size());
  comm.allgather(bodies_per_rank * sizeof(Particle), counts_per_rank.data());

  // 2: use prefix sum to compute displacements
  vector<size_t> beg(comm.size()+1,0), end(comm.size()+1,0);
  for (int i = 0; i < comm.size(); ++i)
  {
    end[i  ] = beg[i] + counts_per_rank[i];       // end of the chunk for rank 'i'
    beg[i+1] = end[i];                            // beginning of the chunk for rank 'i+1'
  }

  // total number of data to write 
  const auto global_count = end[comm.size()-1]; 

  // 3: Before writing data, we write a header. 
  //    ** Only root rank write to avoid dublication
  if (comm.is_root())
  {
    MPI_Status status;
    Header header;
    sprintf(header.model_name, "%s", model_name.c_str());
    header.nbodies = global_count/sizeof(Particle);
    header.comm_size = comm.size();
    header.seed    = seed;

    MPI_File_seek(fh,0,MPI_SEEK_SET);     // write at the beginning of the file

    MPI_File_write(fh,       // file handle
        &header,             // pointer to the header
        sizeof(Header),      // size of the header ..
        MPI_BYTE,            //   .. in bytes
        &status);            // status

    int check_count;
    MPI_Get_count(&status, MPI_BYTE, &check_count); // obtain number of bytes writte

    // if incorrect number of bytes were written, throw exception
    int count = sizeof(Header);
    if (count != check_count)
      throw error_msg(comm.rank(),
          string("FATAL: Incorrect number of header bytes was written. Expected ")+to_string(count)+
          " but wrote " + to_string(check_count) + "!\n");
  }

  // 4: Now write data
  //    ** Since amount of data in parallel can exceed 4GB (1<<31)
  //       and MPI_File_write take count as 'int', we must 
  //       guard for chunk size > 4GB
  size_t chunk_offset = sizeof(Header) + beg[comm.rank()];
  size_t chunk_size   = end[comm.rank()] - beg[comm.rank()];   
  if (chunk_size > 0)
  {
    MPI_File_seek(fh, chunk_offset, MPI_SEEK_SET);
    MPI_Status status;
    const size_t batchMax = (1U << 30) - 1;
    int loop_count = 1;
    size_t offset = 0;
    while (chunk_size > 0)
    {
      const auto count = static_cast<int>(min(chunk_size,batchMax));
      assert(count > 0);
      MPI_File_write(fh,
          local_bodies.data() + offset,
          count,
          MPI_BYTE,
          &status);

      int check_count;
      MPI_Get_count(&status, MPI_BYTE, &check_count);
      if (count != check_count)
        throw error_msg(comm.rank(),
            string("FATAL: Incorrect number of data bytes was written during loop# ") + 
            to_string(loop_count) + ". Expected "+
            to_string(count)+ " but wrote " + to_string(check_count) + "!\n");
      offset     += count;
      chunk_size -= count;
      loop_count++;
    }
  }

  MPI_File_close(&fh);
}

int main(int argc, char * argv[])
{
  using namespace std;
  using namespace parse_arguments;

  my::MPI mpi(argc, argv);

  my::MPI::Comm comm{MPI_COMM_WORLD};

  auto filename = string("test.dat");
  auto do_read  = false;
  auto nbodies  = 1000000;
  auto model_name = string("basic model");

  auto params = pack(argc, argv,
      param("filename", filename, "f", "fname"),
      param("read file [otherwise write]", do_read, "r","read"),
      param("number of bodies [if write]", nbodies, "n", "nbody"),
      param("model name", model_name, "", "name")
      );

  if (!params.parse())
  {
    if (0 == comm.rank())
      cerr << params.usage();
    return 0;
  }
  if (filename.empty())
    if (0 == comm.rank())
    {
      cerr << argv[0] << ":: FATAL: Filename is not provided\n";
      cerr << params.usage();
      return 0;
    }

  if (0 == comm.rank())
    cerr << params.params();

  comm.barrier();

  if (do_read)
    read_bodies(comm,filename);
  else
    write_bodies(comm,filename,nbodies,model_name);

  if (comm.is_root())
    cout << "OK" << endl;



  return 0;
}
