Training materials
==================

This folder contains various simple code examples that can be utilized
for example in HPC training.

Parallel programming
--------------------
Simple parallel programming examples utilizing both message passing with MPI
and shared memory parallelization with OpenMP

