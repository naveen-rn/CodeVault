PRACE CodeVault
===============
PRACE CodeVault is an open repository containing various high
performance computing code samples. The project aims to support
self-learning of HPC programming and will be used as an Open platform
for the HPC community to share example code snippets, proof-of-concept
codes and so forth.  

CodeVault contains training material from PRACE partners, as well as
example codes of common HPC kernels such as dense and sparse linear
algebra, spectral and N-body methods, structured and unstructured
grids, Monte Carlo methods and parallel I/O. The code samples are
published as open source and can be used both for educational purposes
and as parts of real application suites (as permitted by particular
license).  

How to contribute
-----------------
Any contributions (new code samples, bug fixes, improvements etc.) are
warmly welcome. In order to contribute, please follow the standard
Gitlab workflow:

1. Fork the project into your personal space on GitLab.com
2. Create a feature branch
3. Work on your contributions
4. Push the commit(s) to your fork
5. Submit a merge request to the master branch
